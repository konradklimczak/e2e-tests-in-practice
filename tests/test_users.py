import requests
import config
import endpoints


class TestUsers:
    def test_get_users_list(self, new_user_data):
        user_list_response = requests.get(endpoints.users)
        assert user_list_response.status_code == 200
        assert new_user_data in user_list_response.json()

    def test_create_user_and_get_thier_details(self, new_user_data):
        new_user_response = requests.get(
            f'{config.base_url}/users/{new_user_data["id"]}')

        assert new_user_response.status_code == 200

        assert new_user_data == new_user_response.json()

        requests.delete(
            f'{config.base_url}/users/{new_user_data["id"]}')
