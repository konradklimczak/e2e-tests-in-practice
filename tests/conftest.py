import pytest
import requests
from faker import Faker

import config

fake = Faker()


@pytest.fixture
def new_user_data() -> dict:
    expected_email = fake.safe_email()
    create_user_payload = {'email': expected_email}

    create_user_response = requests.post(
        f'{config.base_url}/users', json=create_user_payload)

    assert create_user_response.status_code == 201
    create_user_response_json = create_user_response.json()
    assert create_user_response_json['email'] == expected_email
    assert 'id' in create_user_response_json

    return {'email': expected_email, 'id': create_user_response_json['id']}
