import config

users = f'{config.base_url}/users'


def user_with_id(user_id):
    return f'{users}/{user_id}'
